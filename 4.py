results = []
for i in range(999, 99, -1):
	for j in range(999, 99, -1):
		no = i*j
		if str(no) == str(no)[::-1]:
			results.append(no)
			break	# Theres no point in calculating smaller numbers

print(max(results))