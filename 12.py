from functools import reduce

def number_of_factors(n):
	return len(set(reduce(list.__add__, ([i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0))))

found = False
number = 1
triangle_number = 1

while not found:
	if number_of_factors(triangle_number) > 500:
		print(triangle_number)
		found = True
	else:
		number += 1
		triangle_number += number