limit = 4000000
numbers = (1, 2)
results = (2,)
i = 2
counter = 2

while i <= limit:
	new_number = numbers[counter-1] + numbers[counter-2]
	numbers += (new_number,)
	if new_number % 2 == 0:
		results += (new_number,)
	i = new_number
	counter += 1

print(sum(results))
