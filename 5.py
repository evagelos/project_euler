number = 380
result = None
while not result:
	for i in range(11, 21):
		if number % i == 0:
			if i == 20:
				result = number
				print(result)
		else:
			number += 380
			break


# 20	19 18 17 16 15 14 13 12 11  All numbers that are above this line are divided so theres not point in calculating modulo for them
# 10		9    8      7
# 5		    6
# 4 		3
# 2

# The increase for the loop should the biggest number between 20 and the numbers that would be dividers
# 20-11 -> 220
# 20-12 -> 60
# 20-13 -> 260
# 20-14 -> 140
# 20-15 -> 60
# 20-16 -> 80
# 20-17 -> 340
# 20-18 -> 180
# 20-19 -> 380