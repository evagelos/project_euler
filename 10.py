result = 0
sieve = [True] * 2000000

for i in range(2, 2000000):
    if sieve[i]:
        result += i
        for i in range(i*i, 2000000, i):
            sieve[i] = False

print (result)